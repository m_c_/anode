from typing import List, Optional
import requests
from config import CONFIG


class VideosLister:
    ''' Class for working with videos listing '''

    def get_all_videos(self) -> Optional[List]:
        ''' '''
        if not CONFIG['videos_list'] or not CONFIG['videos_list']['url']:
            return None

        r = requests.get(CONFIG['videos_list']['url'], verify=CONFIG['videos_list']['verify'])
        if r.status_code != 200:
            return None

        resp = r.json()
        if not resp or resp['success'] == False or not resp['data']:
            return None

        return resp['data']
