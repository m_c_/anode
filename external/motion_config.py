from functools import cmp_to_key
from typing import Dict, Optional, List, Tuple
import requests
from config import CONFIG


class MotionConfig:
    ''' Class for working with motion detection config '''

    def get_motion_detection_settings(self, camera_id: int) -> Dict:
        ''' '''
        config = CONFIG['motion']['settings']
        if not config['url']:
            return {}

        params: Dict = {'id': camera_id}
        r = requests.get(config['url'], params=params, verify=config['verify'])
        if r.status_code != 200:
            return {}

        setts: Optional[Dict] = r.json()
        if not setts or setts['success'] == False or not setts['data']:
            return {}

        if 'excludes' in setts['data']:
            setts['data']['excludes'] = self._glue(setts['data']['excludes'])
        return {camera_id: setts['data']}

    def _comparer(self, left: Dict, right: Dict) -> int:
        ''' Compares one rectangle with another '''
        if left['x0'] == right['x0']:
            if left['y0'] == right['y0']:
                return 0
            if left['y0'] < right['y0']:
                return -1
            if left['y0'] > right['y0']:
                return 1
    
        if left['x0'] < right['x0']:
            if left['y0'] > right['y0']:
                return 1
            else:
                return -1
    
        if left['x0'] > right['x0']:
            if left['y0'] < right['y0']:
                return -1
            else:
                return 1
        
        return 0

    def _glue_one(self, exclude: Dict, excludes: List[Dict]) -> Tuple[bool, List[Dict]]:
        ''' Glues one rect to another if they share one side '''
        rect = None
        for ind, e in enumerate(excludes):
            if (
                (exclude['x1'] == e['x0'] and exclude['y0'] == e['y0'] and exclude['y1'] == e['y1']) or
                (exclude['x0'] == e['x0'] and exclude['x1'] == e['x1'] and exclude['y1'] == e['y0'])
            ):
                rect = {
                    'x0': exclude['x0'],
                    'y0': exclude['y0'],
                    'x1': e['x1'],
                    'y1': e['y1']
                }
                excludes[ind] = rect
                break
        if rect is None:
            excludes.insert(0, exclude)
        return True if rect else False, excludes
    
    def _glue(self, excludes: List[Dict]) -> Optional[List[Dict]]:
        ''' Glue rects in list '''
        if not excludes:
            return None
        excludes = sorted(excludes, key=cmp_to_key(self._comparer))
        while True:
            glued = False
            i = 0
            while True:
                try:
                    exclude = excludes.pop(i)
                except IndexError:
                    break
                glued_loc, excludes = self._glue_one(exclude, excludes)
                i += 0 if glued_loc else 1
                if glued_loc:
                    glued = True
            if glued == False:
                break
        return excludes
