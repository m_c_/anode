import os
from typing import Dict, Optional
from config import CONFIG


class VideoRegistryException(Exception):
    ''' '''


class VideoRegistry():
    '''
    Class for managing list of videos from videos.txt (the file)
    videos.txt contains rows like:
    camera_id,rtmp_url
    and ends with empty line
    '''

    @property
    def is_need_save(self) -> bool:
        ''' '''
        return self._need_save

    def __init__(self):
        ''' '''
        dirpath: str = os.path.dirname(os.path.abspath(__file__))
        filepath: str = os.path.join(dirpath, 'videos.txt')
        if not os.path.isfile(filepath):
            open(filepath, 'a').close()

        self._filepath: str = filepath # path to the file with videos
        self._videos: Dict[int, str] = {} # dict {camera_id: camera_url}
        # max number of videos the system can handle
        self._max: int = CONFIG['detection']['gpus_count'] * CONFIG['detection']['nets_per_gpu'] * CONFIG['detection']['videos_per_net']
        self._need_save: bool = False # if list of video changed, but save method was not called

        self._load()

    def _video_to_str(self, vid: int, url: str) -> str:
        ''' Create row for the file '''
        return '{},{}\n'.format(vid, url)

    def _load(self):
        ''' Reads the file to _videos dict '''
        with open(self._filepath, 'r') as f:
            for line in f:
                line = line.split(',')
                if len(line) == 2:
                    self._videos[int(line[0].strip())] = line[1].strip()
        if len(self._videos) > self._max:
            raise VideoRegistryException('Too many video streams')

    def save(self):
        ''' Saves dict of videos to the file '''
        with open(self._filepath, 'w') as f:
            for vid in self._videos:
                f.write(self._video_to_str(vid, self._videos[vid]))
        self._need_save = False

    def add(self, vid: int, url: str) -> bool:
        ''' Adds new video to _videos dict '''
        if vid in self._videos:
            return False
        if len(self._videos) + 1 > self._max:
            raise VideoRegistryException('Too many video streams')
        self._videos[int(vid)] = url
        self._need_save = True
        return True

    def delete(self, vid: int) -> bool:
        ''' Deletes video from _videos dict '''
        if vid not in self._videos:
            return False
        del self._videos[vid]
        self._need_save = True
        return True

    def list_video(self) -> Dict:
        ''' Returns _video dict '''
        return self._videos.copy()

    def get_video_count(self) -> int:
        ''' '''
        return len(self._videos)
