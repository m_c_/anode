# do not modify, use config.py
from typing import Dict


CONFIG: Dict = {
    'debug': False,
    'console': False,
    'log': '/var/log/anode.log',
    'logger': 'anode_logger',
    'screenshot_sizes': (292, 164),
    'max_retries': 50, # for reading video
    'videos_list': {
        'url': None,
        'verify': True
    },
    'detection': {
        'gpus_count': 1,
        'input_queue': 'detection_source',
        'output_queue': 'image_source',
        'nets_per_gpu': 1, # will be copied from current
        'videos_per_net': 1, # will be copied from current
        'current': 'yolo3',
        'yolo3': {
            'nets_per_gpu': 5,
            'videos_per_net': 10,
            'cfg': '',
            'weights': '',
            'metadata': '',
        },
    },
    'web': {
        'port': 8080,
        'allowed_ips': ('127.0.0.1',)
    },
    'motion': {
        'min_motion_area': 500,
        'delta_threshold': 5,
        'skip_gap': 20,
        'output_queue': 'motion_result',
        'settings':  {
            'url': None,
            'verify': True
        }
    },
    'queue': {
        'user': 'guest',
        'pass': 'guest',
        'host': '127.0.0.1',
        'port': 5672,
        'blocked_timeout': 5 # seconds
    },
    'comparer': {
        'diff': 0.6,
        'expire': 60, # in seconds
        'input_queue': 'image_source',
        'output_queue': 'image_result'
    }
}
CONFIG['detection']['nets_per_gpu'] = CONFIG['detection'][CONFIG['detection']['current']]['nets_per_gpu']
CONFIG['detection']['videos_per_net'] = CONFIG['detection'][CONFIG['detection']['current']]['videos_per_net']
