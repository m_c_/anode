from collections import namedtuple
from typing import Optional, Tuple, List
from multiprocessing import Queue
from queue import Empty
import subprocess, base64
import cv2, imutils
from config import CONFIG


# namedtuple for passing from video reader to motion and object detection
VideoItem = namedtuple('VideoItem', ('camera_id', 'date_time', 'frame'))

# namedtuple for passing from object detection to image comparer
DbDataItem = namedtuple('DbDataItem', ('camera_id', 'date_time', 'type', 'coords', 'data'))

# namedtuple for passing from detector
DetectedObjectItem = namedtuple('DetectedObjectItem', ('x', 'y', 'width', 'height', 'category'))

# namedtuple for passing commands from web to main process
Command = namedtuple('Command', ('type', 'vid', 'url'))

COMMAND_TYPE_ADD_VIDEO = 1
COMMAND_TYPE_DELETE_VIDEO = 2
COMMAND_TYPE_UPDATE_MOTION = 3
COMMAND_TYPE_UPDATE_VIDEOS = 4


def clear_queue(queue: Queue):
    '''
    There is no guarantee that items will be in queue when getting, so
    should check couple times 
    '''
    for _ in range(2):
        while True:
            try:
                queue.get_nowait()
            except Empty:
                break


def decode_fourcc(v) -> Optional[str]:
    ''' Converts codec name to string '''
    try:
        v = int(v)
    except ValueError:
        return None
    return ''.join([chr((v >> 8 * i) & 0xFF) for i in range(4)])


def extract_card(stat: str, pos: int) -> Tuple[str, str]:
    ''' Extracts memory usage and % utilization about one GPU '''
    gpu_mem = ''
    gpu_util = ''
    tmp = stat[pos].split('|')
    if len(tmp) > 2:
        gpu_mem = tmp[2].strip()
    if len(tmp) > 3:
        gpu_util = tmp[3].split('%')[0].strip() + '%'
    return gpu_mem, gpu_util


def get_card_stat() -> Tuple[List, List]:
    '''
    Extracts memory usage and % utilization of all GPUs
    by parsing nvidis-smi output 
    '''
    gpu_mem = []
    gpu_util = []
    out = subprocess.check_output('nvidia-smi', encoding='utf-8')
    out = out.split('\n')
    if len(out) > 8:
        m, u = extract_card(out, 8)
        gpu_mem.append(m)
        gpu_util.append(u)
    if len(out) > 11:
        m, u = extract_card(out, 11)
        gpu_mem.append(m)
        gpu_util.append(u)
    return gpu_util, gpu_mem


def get_cam_stat(url: str):
    '''
    Returns dict with data about video stream from url
    @return: False or dict
    '''
    cap = cv2.VideoCapture(url)
    if cap.isOpened():
        i = 0
        while i <= CONFIG['max_retries']:
            ret, frame = cap.read()
            if ret:
                frame = imutils.resize(frame, width=CONFIG['screenshot_sizes'][0])
                im = base64.b64encode(cv2.imencode('.jpg', frame)[1].tostring())
                cap.release()
                return {
                    'screen': im.decode(),
                    'fps': cap.get(cv2.CAP_PROP_FPS),
                    'width': cap.get(cv2.CAP_PROP_FRAME_WIDTH),
                    'height': cap.get(cv2.CAP_PROP_FRAME_HEIGHT),
                    'codec': decode_fourcc(cap.get(cv2.CAP_PROP_FOURCC))
                }
            else:
                i += 1
        cap.release()
    return False
