import os, signal, logging
from math import ceil
from time import sleep
from queue import Empty
from multiprocessing import Queue, Process
from typing import List, NamedTuple, Optional
from setproctitle import setproctitle
from config import CONFIG
from registry import VideoRegistry, VideoRegistryException
from workers.detector import ObjectDetector
from workers.web import Server
from workers.reader import VideoReader
from workers.motion import MotionDetector
from workers.comparer import Comparer
from queues.rabbitmq import QueueClient
from common import Command, COMMAND_TYPE_ADD_VIDEO, COMMAND_TYPE_DELETE_VIDEO, COMMAND_TYPE_UPDATE_MOTION, COMMAND_TYPE_UPDATE_VIDEOS
from external.videos_lister import VideosLister


logger = logging.getLogger(CONFIG['logger'])
logger.setLevel(logging.DEBUG if CONFIG['debug'] else logging.WARNING)
formatter = logging.Formatter('%(asctime)s %(message)s')
if CONFIG['console']:
    handler = logging.StreamHandler()
else:
    handler = logging.FileHandler(CONFIG['log'])
handler.setFormatter(formatter)
logger.addHandler(handler)


class Commander():
    '''
    Class starts all other processes, restarts them if needed
    '''

    @property
    def title(self) -> str:
        ''' '''
        return 'Main process {}'.format(os.getpid())

    def __init__(self):
        ''' '''
        self._stop: bool = False # flag for stop working
        self._check_videos: bool = False # flag for compare videos with processes
        self._check_children: bool = False # flag for restarting child process
        self._processes: List[Process] = [] # list of child processes
        self._gpus: List[int] = [0] # list with number of nnets on gpus
        self._queues: List[Queue] = [] # list of queues between video readers and motion detectors
        self._signals: List = [signal.SIGQUIT, signal.SIGTERM, signal.SIGCHLD]
        self._command_queue: Queue = None # queue for commands from web
        self._video_registry: VideoRegistry = None # object for storing list of videos

    def run(self):
        '''Here starts everything'''

        setproctitle('Anode main process')
        
        for sig in self._signals:
            signal.signal(sig, self._signal_handler)

        logger.debug('Commander process started')

        # list of GPUs
        self._gpus = self._gpus * CONFIG['detection']['gpus_count']

        # list of video urls
        try:
            self._video_registry = VideoRegistry()
            videos = self._video_registry.list_video()
        except VideoRegistryException as e:
            logger.exception(e)
            exit()

        # creating queues
        client = QueueClient()
        client.init_queues((
            CONFIG['motion']['output_queue'],
            CONFIG['detection']['input_queue'],
            CONFIG['detection']['output_queue'],
            CONFIG['comparer']['output_queue']
        ))
        client.close()
        logger.debug('Queues created')

        # web server
        self._command_queue = Queue(50)
        try:
            self._processes.append(Server(self._command_queue))
        except Exception as e:
            logger.exception(e)
            exit()
        logger.debug('Web-server created')

        # processes with NNets
        ind = 0
        for _ in range(self._need_detectors_count()):
            try:
                self._processes.append(ObjectDetector(ind))
                self._gpus[ind] += 1
                if ind + 1 == len(self._gpus):
                    ind = 0
                else:
                    ind += 1
            except Exception as e:
                logger.exception(e)
        logger.debug('Detectors created count: {}'.format(sum(self._gpus)))

        # process with image compare and filter
        self._processes.append(Comparer())
        logger.debug('Images comparer created')

        # video reading processes and process with motion detection
        cnt = 0
        self._queues = {}
        for vid in videos:
            try:
                self._prepare_new_processes(vid, videos[vid])
                cnt += 1
            except Exception as e:
                logger.exception(e)
        logger.debug('Reading processes and Motion detectors created count: {}'.format(cnt))

        for p in self._processes:
            p.start(self._signals)

        while not self._stop:
            #
            if self._check_children:
                for p in self._children_to_restart():
                    p.start(self._signals)
                self._check_children = False

            # get commands to registry
            self._process_command()

            # if list of videos changed
            if self._video_registry.is_need_save:
                self._update_videos_processes()
            else:
                sleep(3)

        logger.debug('Main cycle stopped')
        for p in self._processes:
            p.stop()
        for p in self._processes:
            p.join()

    def _signal_handler(self, sig: int, frame):
        ''' Sets some flags to process in main loop '''
        logger.debug('{} received {} signal'.format(self.title, sig))
        if sig == signal.SIGQUIT or sig == signal.SIGTERM: # 3 or 15
            self._stop = True
        elif sig == signal.SIGCHLD: # 17
            if not self._stop:
                self._check_children = True

    def _children_to_restart(self) -> List:
        ''' Checks child processes needing to restart '''
        children: List = []
        if not self._stop:
            for p in self._processes:
                if not p.is_alive:
                    children.append(p)
        return children

    def _need_detectors_count(self) -> int:
        ''' Return number of needed object detectors '''
        cnt: int = ceil(self._video_registry.get_video_count() / CONFIG['detection']['videos_per_net'])
        if cnt == 0:
            return 1
        max_nets = CONFIG['detection']['gpus_count']*CONFIG['detection']['nets_per_gpu']
        if cnt > max_nets:
            return max_nets
        return cnt

    def _count_detectors(self) -> int:
        ''' Count created object detectors '''
        cnt: int = 0
        for p in self._processes:
            if isinstance(p, ObjectDetector):
                cnt += 1
        return cnt

    def _update_motion(self, command: Command):
        ''' Sends signal to MotionDetector to update settings '''
        for p in self._processes:
            if isinstance(p, MotionDetector) and p.vid == command.vid:
                os.kill(p.pid, signal.SIGHUP)
                break

    def _update_videos_list(self):
        ''' Get all videos and update registry '''
        vl = VideosLister()
        try:
            new_videos: Optional[List] = vl.get_all_videos()
        except Exception as e:
            logger.exception(e)
            return
        if new_videos is None:
                return

        old_videos: Dict = self._video_registry.list_video()
        for vid in old_videos:
            self._video_registry.delete(vid)

        for video in new_videos:
            try:
                self._video_registry.add(video['id'], video['url'])
            except VideoRegistryException:
                pass

    def _prepare_new_processes(self, vid: int, url: str):
        ''' Creates processes for new video stream '''
        self._queues[vid] = Queue(50)
        self._processes.append(VideoReader(vid, url, self._queues[vid]))
        self._processes.append(MotionDetector(vid, self._queues[vid]))

    def _process_command(self):
        ''' '''
        cycle = 0
        while (cycle < 11):
            try:
                command = self._command_queue.get_nowait()
                logger.debug('Main received command: {}'.format(command.type))
                cycle += 1
                if command.type == COMMAND_TYPE_ADD_VIDEO:
                    try:
                        self._video_registry.add(command.vid, command.url)
                    except VideoRegistryException:
                        pass
                elif command.type == COMMAND_TYPE_DELETE_VIDEO:
                    self._video_registry.delete(command.vid)
                elif command.type == COMMAND_TYPE_UPDATE_MOTION:
                    self._update_motion(command)
                elif command.type == COMMAND_TYPE_UPDATE_VIDEOS:
                    self._update_videos_list()
            except Empty:
                break

    def _update_videos_processes(self):
        ''' '''
        self._video_registry.save()
        videos: Dict = self._video_registry.list_video()

        # adding new readers
        for vid in videos:
            found = False
            for p in self._processes:
                if isinstance(p, VideoReader) and p.vid == vid:
                    found = True
                    break
            if not found:
                self._prepare_new_processes(vid, videos[vid])

        # new detectors if needed
        dcnt = self._count_detectors()
        ncnt = self._need_detectors_count()
        while ncnt > dcnt:
            ind = self._gpus.index(min(self._gpus))
            self._processes.append(ObjectDetector(ind))
            self._gpus[ind] += 1
            dcnt += 1

        # starting
        for p in self._processes:
            if not p.is_started:
                p.start(self._signals)

        # deleting unneeded processes
        videos: List = videos.keys()
        to_delete: List = []
        vids: List = []
        for p in self._processes:
            if (isinstance(p, VideoReader) or isinstance(p, MotionDetector)) and p.vid not in videos:
                to_delete.append(p)
                vids.append(p.vid)
        for p in to_delete:
            self._processes.remove(p)
            p.stop()
            p.join()
        for vid in vids:    
            if vid in self._queues:
                self._queues[vid].close()
                del self._queues[vid]


if __name__ == '__main__':
    Commander().run() 
