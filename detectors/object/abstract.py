from abc import ABC, abstractmethod
from typing import Optional, List
from common import DetectedObjectItem


class AbstractObjectDetector(ABC):
    ''' Abstract parent for object detectors '''

    @abstractmethod
    def init_detector(self, gpu_num: Optional[int]):
        ''' '''

    @abstractmethod
    def detect(self, frame) -> List[DetectedObjectItem]:
        ''' '''
