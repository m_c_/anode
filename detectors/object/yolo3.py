from typing import Optional, List, Dict
from pydarknet import Detector as Yolo3Detector, Image, set_cuda_device
from config import CONFIG
from detectors.object.abstract import AbstractObjectDetector
from common import DetectedObjectItem


class Detector(AbstractObjectDetector):
    ''' Class for detecting objects '''

    _nnet: Yolo3Detector
    _categories: Dict[str, int] = { # object categories detected
        'person': 0,
        'car': 1,
        'bus': 2,
        'motorbike': 3,
        'cat': 4,
        'dog': 5
    }

    def init_detector(self, gpu_num: Optional[int]):
        ''' '''
        set_cuda_device(gpu_num)
        config = CONFIG['detection']['yolo3']
        self._nnet = Yolo3Detector(
            bytes(config['cfg'], encoding='utf-8'),
            bytes(config['weights'], encoding='utf-8'),
            0,
            bytes(config['metadata'], encoding='utf-8')
        )

    def detect(self, frame) -> List[DetectedObjectItem]:
        ''' '''
        results = self._nnet.detect(Image(frame))
        return [self._convert_detected(res) for res in results]

    def _convert_detected(self, res: List) -> DetectedObjectItem:
        ''' Formats detection results '''
        orig_x, orig_y, orig_w, orig_h = res[2]
        x: int = int(orig_x - orig_w / 2)
        y: int = int(orig_y - orig_h / 2)
        x = x if x > 0 else 0
        y = y if y > 0 else 0
        cat: str = res[0].decode('utf-8')
        return DetectedObjectItem(
            x,
            y,
            int(orig_x + orig_w / 2) - x,
            int(orig_y + orig_h / 2) - y,
            self._categories[cat] if cat in self._categories else -1
        )
