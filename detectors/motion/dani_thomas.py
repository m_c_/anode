# https://bitbucket.org/dani_thomas/multimotiondetect/src/75b981aa3e806ee80c5cf7f759bd24a6b0ec5d60/multiMotionDetect.py?at=master&fileviewer=file-view-default
from typing import Optional, List, Dict
import cv2
import imutils
from config import CONFIG
from detectors.motion.abstract import AbstractMotionDetector


class Detector(AbstractMotionDetector):
    ''' Class for detecting motion '''

    _average: Optional[float] = None

    def init_detector(self, video_id: int):
        ''' '''
        super().init_detector(video_id)
        self._average = None

    def detect(self, frame) -> Optional[List[Dict]]:
        ''' '''
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        if self._average is None:
            self._average = gray.copy().astype('float')
            return None
        else:
            cv2.accumulateWeighted(gray, self._average, 0.5)
            frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(self._average))
            thresh = cv2.threshold(frameDelta, CONFIG['motion']['delta_threshold'], 255, cv2.THRESH_BINARY)[1]
            thresh = cv2.dilate(thresh, None, iterations=2)
            contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = imutils.grab_contours(contours)
            res: List[Dict] = []
            for contour in contours:
                if cv2.contourArea(contour) < self._min_motion_area:
                    continue
                res.append(dict(zip(['x', 'y', 'width', 'height'], cv2.boundingRect(contour))))
            if self._excludes:
                res = [
                    rect for rect in res if not self._have_overlap(rect, self._excludes)
                ]
            res = self._glue(res)
            return res
