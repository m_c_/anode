import logging
from abc import ABC, abstractmethod
from typing import Optional, List, Dict, Tuple
from config import CONFIG
from external.motion_config import MotionConfig


logger = logging.getLogger(CONFIG['logger'])


class AbstractMotionDetector(ABC):
    ''' Abstract parent for motion detectors '''

    _min_motion_area: int = 0
    _excludes: Optional[List[Dict]] = None

    @abstractmethod
    def detect(self, frame) -> Optional[List[Dict]]:
        ''' '''

    def init_detector(self, video_id: int):
        ''' '''
        self._min_motion_area = CONFIG['motion']['min_motion_area']
        self._excludes = []

        try:
            settings = MotionConfig().get_motion_detection_settings(video_id)
            if video_id in settings:
                if 'excludes' in settings[video_id]:
                    self._excludes = settings[video_id]['excludes']
                if 'min_motion_area' in settings[video_id]:
                    self._min_motion_area = settings[video_id]['min_motion_area']
        except Exception as e:
            logger.exception(e)

    def _have_overlap(self, rect: Dict, excludes: List[Dict]) -> bool:
        '''
        https://math.stackexchange.com/questions/99565/simplest-way-to-calculate-the-intersect-area-of-two-rectangles
        '''
        size: int = 0
        x1: int = rect['x'] + rect['width']
        y1: int = rect['y'] + rect['height']
        for excl in excludes:
            x_overlap = max(0, min(x1, excl['x1']) - max(rect['x'], excl['x0']))
            y_overlap = max(0, min(y1, excl['y1']) - max(rect['y'], excl['y0']))
            if x_overlap and y_overlap:
                size += x_overlap * y_overlap
        return False if size < rect['width'] * rect['height'] else True

    def _glue(self, res: List[Dict]) -> List[Dict]:
        ''' Glue rectangles if intersected '''
        while True:
            glued: bool = False
            i: int = 0
            while True:
                try:
                    r: Dict = res.pop(i)
                    g, res = self._glue_one(r, res)
                    if g:
                        glued = True
                    i += 1
                except IndexError:
                    break
            if glued == False:
                break
        return res

    def _glue_one(self, rect: Dict, rects: List[Dict]) -> Tuple:
        ''' '''
        gap: int = CONFIG['motion']['skip_gap']
        total: bool = False
        x1: int = rect['x'] + rect['width']
        y1: int = rect['y'] + rect['height']
        while True:
            tmp: List = []
            glued: bool = False
            for r in rects:
                rx1: int = r['x'] + r['width']
                ry1: int = r['y'] + r['height']
                x_overlap = max(
                    0,
                    min(x1 + gap, rx1 + gap) - max(rect['x'] - gap, r['x'] - gap)
                )
                y_overlap = max(
                    0,
                    min(y1 + gap, ry1 + gap) - max(rect['y'] - gap, r['y'] - gap)
                )
                if (x_overlap and y_overlap):
                    x1 = max(x1, rx1)
                    y1 = max(y1, ry1)
                    rect['x'] = min(rect['x'], r['x'])
                    rect['y'] = min(rect['y'], r['y'])
                    rect['width'] = x1 - rect['x']
                    rect['height'] = y1 - rect['y']
                    glued = True
                    total = True
                else:
                    tmp.append(r)
            rects = tmp
            if not glued:
                break
        rects.append(rect)
        return total, rects
