import logging, signal
from queue import Empty
from multiprocessing import Queue
from setproctitle import setproctitle
from config import CONFIG
from workers.abstract import AbstractWorker
from queues.rabbitmq import QueueClient
from detectors.motion.dani_thomas import Detector


logger = logging.getLogger(CONFIG['logger'])


class MotionDetector(AbstractWorker):
    ''' Process with motion detection '''

    _video_id: int = 0 # video id
    _internal_queue = None # internal queue for video frames
    _update_params: bool = False # flag for signal

    def __init__(self, video_id: int, queue: Queue):
        ''' '''
        super().__init__()
        self._video_id = video_id
        self._internal_queue = queue
        self._signals.append(signal.SIGHUP)

    @property
    def vid(self) -> int:
        '''  Id of video '''
        return self._video_id

    def _run(self):
        ''' Real work '''
        
        setproctitle('Anode motion detection process for {}'.format(self._video_id))
        
        detector = Detector()
        detector.init_detector(self._video_id)

        qclient = QueueClient()

        while not self._stop:
            try:
                item = self._internal_queue.get(True, 5)

                results = detector.detect(item.frame)
                if results:
                    # to object detection
                    putted = qclient.put(CONFIG['detection']['input_queue'], item)

                    # to results queue
                    item = {'cameraId': item.camera_id, 'date': item.date_time.strftime('%Y-%m-%d %H:%M:%S'), 'data': results}
                    putted = qclient.put(CONFIG['motion']['output_queue'], item, False)
                else:
                    qclient.process_events()

                if self._update_params:
                    detector.init_detector(self._video_id)
                    self._update_params = False

            except Empty:
                qclient.process_events()
            except Exception as e:
                logger.exception(e)

        qclient.close()
        logger.debug('{} stopped cycle'.format(self.title))

    def _signal_handler(self, sig: int, frame):
        ''' Sets some flags to process in main loop '''
        super()._signal_handler(sig, frame)
        if sig == signal.SIGHUP:
            self._update_params = True
