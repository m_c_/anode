import logging
from datetime import datetime
from setproctitle import setproctitle
from base64 import b64encode
import cv2
from image_match.goldberg import ImageSignature
from workers.abstract import AbstractWorker
from config import CONFIG
from queues.rabbitmq import QueueClient


logger = logging.getLogger(CONFIG['logger'])


class Comparer(AbstractWorker):
    ''' Gets detected objects, compares them and saves if differrent '''

    _sig = None # object for creating image signatures
    _qclient = None # client for system queue
    _images = None # dict of lists of dicts {'item': DbDataItem, 'sig': signature}

    def _run(self):
        ''' Real work '''

        setproctitle('Anode object comparer process')

        self._sig = ImageSignature()
        self._qclient = QueueClient()
        self._images = {} # contains lists of dicts {'item': DbDataItem, 'sig': signature}
        remove = 15

        while not self._stop:
            item = self._qclient.get(CONFIG['comparer']['input_queue'])
            if item:
                to_store = {
                    'item': item,
                    'sig': self._sig.generate_signature(item.data)
                }
                if item.camera_id not in self._images:
                    self._images[item.camera_id] = [to_store]
                else:
                    similar = False
                    for it in self._images[item.camera_id]:
                        if it['item'].type == item.type and self._is_similar(it['sig'], to_store['sig']):
                            similar = True
                            break
                    if not similar:
                        self._images[item.camera_id].append(to_store)
                remove -= 1
            else:
                self._qclient.process_events()
            if remove == 0:
                self._remove_expire()
                remove = 15

        for cid in self._images:
            for item in self._images[cid]:
                self._put(item['item'])

        self._qclient.close()
        logger.debug('{} stopped cycle'.format(self.title))

    def _is_similar(self, sig1, sig2):
        '''
        Checks if images more or less similar
        @param sig1: image signature
        @param sig2: image signature
        @return: boolean
        '''
        return True if self._sig.normalized_distance(sig1, sig2) < CONFIG['comparer']['diff'] else False

    def _remove_expire(self):
        '''
        If there was no same image for some time then stored image goes to saving
        '''
        now = datetime.utcnow()
        for cid in self._images:
            for item in self._images[cid]:
                if (now - item['item'].date_time).seconds >= CONFIG['comparer']['expire']:
                    self._images[cid].remove(item)
                    self._put(item['item'])

    def _put(self, item):
        '''
        Puts data to output queue
        @param item: DbDataItem
        '''
        self._qclient.put(
            CONFIG['comparer']['output_queue'],
            {
                'cameraId': item.camera_id,
                'date': item.date_time.strftime('%Y-%m-%d %H:%M:%S'),
                'type': item.type,
                'coords': item.coords,
                'data': b64encode(cv2.imencode('.jpg', item.data)[1].tostring()).decode('utf-8')
            },
            False
        )
