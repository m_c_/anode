import logging
from datetime import datetime, timedelta
from time import sleep
from queue import Full
from multiprocessing import Queue
import cv2
from setproctitle import setproctitle
from config import CONFIG
from workers.abstract import AbstractWorker
from common import VideoItem, clear_queue


logger = logging.getLogger(CONFIG['logger'])


class VideoReader(AbstractWorker):
    ''' Class for reading video frames '''

    @property
    def vid(self) -> int:
        ''' '''
        return self._video_id

    @property
    def title(self) -> str:
        ''' '''
        return '{} with video {}'.format(super().title, self._video_id)

    def __init__(self, video_id: int, url: str, queue: Queue):
        ''' '''
        super().__init__()
        self._video_id: int = video_id # video id
        self._url: str = url # video url
        self._queue: Queue = queue # internal queue object

    def _run(self):
        ''' Real work '''

        setproctitle('Anode video reader process for {}'.format(self._video_id))

        cap = self._connect()
        if cap is None:
            return
        
        logger.debug('{} opened url "{}"'.format(self.title, self._url))
        last = self._get_now()
        read_errors = 0
        error: str = ''
        while not self._stop:
            try:
                ret, frame = cap.read()
                if ret:
                    now = self._get_now()
                    if last != now:
                        last = now
                        read_errors = 0
                        try:
                            self._queue.put(VideoItem(self._video_id, now, frame), True, 1)
                        except Full:
                            pass
                else:
                    if not cap.isOpened():
                        error = 'Url "{}" closed in {}'.format(self._url, self.title)
                    else:
                        read_errors += 1
                        if read_errors > CONFIG['max_retries']:
                            error = 'Too many read errors in {}'.format(self.title)
                    if error:
                        logger.error(error)
                        cap.release()
                        sleep(3)
                        cap = self._connect()
                        read_errors = 0
                        error = ''
            except Exception as e:
                logger.exception(e)

        if cap:
            cap.release()

        clear_queue(self._queue)
        logger.debug('{} stopped cycle'.format(self.title))

    def _connect(self):
        '''
        Connects to video stream
        @return: opencv VideoCapture object or None
        '''
        cap = None
        while not self._stop and cap is None:
            try:
                cap = cv2.VideoCapture(self._url)
                if not cap.isOpened():
                    logger.error('{} can not open url "{}"'.format(self.title, self._url))
                    cap = None
            except Exception as e:
                logger.exception(e)
                cap = None
            if cap is None:
                sleep(5)
        return cap        

    def _get_now(self) -> datetime:
        ''' Counts datetime for video frame '''
        return (datetime.utcnow() - timedelta(seconds=2)).replace(microsecond=0)
