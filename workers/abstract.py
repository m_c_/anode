import os, signal, logging
from multiprocessing import Process
from abc import ABC, abstractmethod
from typing import List 
from config import CONFIG


logger = logging.getLogger(CONFIG['logger'])


class AbstractWorker(ABC):
    ''' Parent class for all processes '''

    @property
    def pid(self) -> int:
        ''' '''
        return self._worker.pid if self._worker and self._worker.pid else 0

    @property
    def title(self) -> str:
        ''' '''
        return 'Class {} in process {}'.format(type(self), os.getpid())

    @property
    def is_started(self) -> bool:
        ''' '''
        return False if self._worker is None else True

    @property
    def is_alive(self) -> bool:
        ''' '''
        return self._worker.is_alive() if self._worker else False

    def __init__(self, *args):
        ''' '''
        self._stop: bool = False
        self._worker = None # Process object
        self._signals: List[int] = [signal.SIGQUIT, signal.SIGTERM]

    def stop(self):
        ''' Sends `stop` for worker '''
        if self._worker:
            self._worker.terminate()
        logger.debug('{} send stop command'.format(self.title))

    def join(self):
        ''' Waits for worker to join '''
        if self._worker:
            self._worker.join()

    def run(self, parent_signals: List):
        '''
        function is running in another process
        all infinite loops should run like this:
        while not self._stop:
            # work to do
        '''
        logger.debug('{} runned'.format(self.title))
        for sig in parent_signals:
            signal.signal(sig, signal.SIG_IGN)
        for sig in self._signals:
            signal.signal(sig, self._signal_handler)
        try:
            self._run()
        except Exception as e:
            logger.exception(e)
        exit()

    def start(self, parent_signals: List):
        ''' Start new process with `run` method in it '''
        self._stop = False
        self._worker = Process(target=self.run, args=(parent_signals,))
        self._worker.start()
        logger.debug('{} started'.format(self.title))

    @abstractmethod
    def _run(self):
        ''' All real work in child classes '''

    def _signal_handler(self, sig: int, frame):
        ''' Sets some flags to process in main loop '''
        logger.debug('{} received {} signal'.format(self.title, sig))
        if sig == signal.SIGQUIT or sig == signal.SIGTERM:
            self._stop = True
