import os, logging, asyncio, signal
import psutil, imutils
from aiohttp import web
from aiohttp.web_runner import GracefulExit
from setproctitle import setproctitle
from config import CONFIG
from workers.abstract import AbstractWorker
from registry import VideoRegistry, VideoRegistryException
from common import get_card_stat, get_cam_stat
from common import clear_queue, Command, COMMAND_TYPE_ADD_VIDEO, COMMAND_TYPE_DELETE_VIDEO, COMMAND_TYPE_UPDATE_MOTION, COMMAND_TYPE_UPDATE_VIDEOS


logger = logging.getLogger(CONFIG['logger'])


class Server(AbstractWorker):
    '''
    Process with web server for communicating with outer world
    '''

    _queue = None # queue for commands 

    def __init__(self, queue, *args):
        '''
        @param queue: Queue
        '''
        super().__init__(*args)
        self._queue = queue

    async def _add_video(self, request):
        '''
        Adds video to internal queue
        @param request: Request object
        @return: json response
        '''
        try:
            if not self._is_ip_allowed(request):
                return self._return_error('Access denied')
            vid, url = request.query.get('id', None), request.query.get('url', None)
            if vid and url:
                self._queue.put(Command(COMMAND_TYPE_ADD_VIDEO, int(vid), url))
                return self._return_success()
            return self._return_error('No id and/or url')
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    async def _delete_video(self, request):
        '''
        Deletes video from internal queue
        @param request: Request object
        @return: json response
        '''
        try:
            if not self._is_ip_allowed(request):
                return self._return_error('Access denied')
            vid = request.query.get('id', None)
            if vid:
                self._queue.put(Command(COMMAND_TYPE_DELETE_VIDEO, int(vid), None))
                return self._return_success()
            return self._return_error('No id')
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    async def _update_videos(self, request):
        '''
        Update list of videos
        @param request: Request object
        @return: json response
        '''
        try:
            if not self._is_ip_allowed(request):
                return self._return_error('Access denied')
            self._queue.put(Command(COMMAND_TYPE_UPDATE_VIDEOS, None, None))
            return self._return_success()
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    async def _get_stat(self, request):
        '''
        Get system stats
        @param request: Request object
        @return: json response
        '''
        try:
            gpu_util, gpu_mem = get_card_stat()
            data = {
                'gpu_mem': gpu_mem,
                'gpu_util': gpu_util,
                'load_average': str(os.getloadavg()),
                'video_count': VideoRegistry().get_video_count(),
                'memory_available': '{} MB'.format(
                    psutil.virtual_memory().available / 1024 / 1024
                )
            }
            return self._return_success(data)
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    async def _update_motion_settings(self, request):
        '''
        Sends signal to update motion settings
        @param request: Request object
        @return: json response
        '''
        try:
            vid = request.query.get('id', None)
            if vid:
                self._queue.put(Command(COMMAND_TYPE_UPDATE_MOTION, int(vid), None))
            return self._return_success()
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    async def _get_screenshot(self, request):
        '''
        Returns screen and data from url
        @param request: Request object
        @return: json response
        '''
        url = request.query.get('url', None)
        if not url:
            return self._return_error('Provide url')
        try:
            res = get_cam_stat(url)
            if res == False:
                return self._return_error('Cannot read from camera')
            return self._return_success(res)
        except Exception as e:
            logger.exception(e)
            return self._return_error('Something went wrong')

    def _run(self):
        '''
        Starting server
        '''
        app = web.Application(debug=CONFIG['debug'], logger=logger)
        app.add_routes([
            web.get('/add-video', self._add_video),
            web.get('/delete-video', self._delete_video),
            web.get('/get-stat', self._get_stat),
            web.get('/get-screenshot', self._get_screenshot),
            web.get('/update-motion-settings', self._update_motion_settings),
            web.get('/update-videos', self._update_videos)
        ])
        app.on_startup.append(self._start_background_tasks)
        app.on_cleanup.append(self._cleanup_background_tasks)
        setproctitle('Anode webserver process')
        web.run_app(
            app,
            port=CONFIG['web']['port'],
            access_log=logger if CONFIG['debug'] else None
        )

    async def _start_background_tasks(self, app):
        '''
        Starts checking global stop flag
        @param app: Application object from aiohttp
        '''
        app['commands_listener'] = app.loop.create_task(self._exec_command(app))

    async def _cleanup_background_tasks(self, app):
        '''
        Stops checking global stop flag
        @param app: Application object from aiohttp
        '''
        clear_queue(self._queue)
        app['commands_listener'].cancel()
        await app['commands_listener']

    async def _exec_command(self, app):
        '''
        @param app: Application object from aiohttp
        '''
        while not self._stop:
            await asyncio.sleep(5)

        # @TODO?
        logger.debug('{} stopped cycle'.format(self.title))
        raise GracefulExit()

    def _is_ip_allowed(self, request):
        '''
        Checks if request from allowed IP
        @param request: Request object
        @return: boolean
        '''
        if request.remote in CONFIG['web']['allowed_ips']:
            return True
        return False

    def _return_success(self, data=None):
        '''
        Returns success json response
        @param data: dict
        @return: json response
        '''
        res = {'success': True, 'message': None}
        if data:
            data.update(res)
            return web.json_response(data)
        return web.json_response(res)

    def _return_error(self, message):
        '''
        Returns error json response
        @param message: string
        @return: json response
        '''
        return web.json_response({
            'success': False,
            'message': message
        })
