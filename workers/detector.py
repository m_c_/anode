import logging, time
from typing import Optional
from setproctitle import setproctitle
from config import CONFIG
from workers.abstract import AbstractWorker
from common import DbDataItem
from queues.rabbitmq import QueueClient
from detectors.object.yolo3 import Detector


logger = logging.getLogger(CONFIG['logger'])


class ObjectDetector(AbstractWorker):
    ''' Process with object detection '''

    _qclient: Optional[QueueClient] = None # client for system queue
    _gpu_num: int = 0 # num of GPU to work on

    def __init__(self, gpu_num: int, *args):
        ''' '''
        super().__init__(*args)
        self._gpu_num = gpu_num

    def _run(self):
        ''' Real work '''

        setproctitle('Anode object detection process')

        detector = Detector()
        detector.init_detector(self._gpu_num)

        self._qclient = QueueClient()

        while not self._stop:
            item = self._qclient.get(CONFIG['detection']['input_queue'])
            if item is None:
                self._qclient.sleep(1)
                continue

            results = detector.detect(item.frame)
            for res in results:
                self._qclient.put(
                    CONFIG['detection']['output_queue'],
                    DbDataItem(
                        item.camera_id,
                        item.date_time,
                        res.category,
                        {'x': res.x, 'y': res.y, 'width': res.width, 'height': res.height},
                        item.frame[res.y:res.y + res.height, res.x:res.x + res.width]
                    )
                )
        self._qclient.close()
        logger.debug('{} stopped cycle'.format(self.title))
