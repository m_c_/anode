import cv2
from config import CONFIG
from detectors.object.yolo3 import Detector


class MinTestCommander():
    ''' '''

    def output_image(self, frame, results):
        ''' '''
        for res in results:
            x, y, w, h = bounds
            cv2.rectangle(
                frame,
                (int(res.x - res.width / 2), int(res.y - res.height / 2)),
                (int(res.x + res.width / 2), int(res.y + res.height / 2)),
                (255, 0, 0)
            )
            cv2.putText(
                frame,
                res.category,
                (res.x, res.y),
                cv2.FONT_HERSHEY_COMPLEX,
                1,
                (255, 255, 0)
            )
        cv2.imshow('preview', frame)

    def run(self):
        ''' '''
        net = Detector()
        net.init_detector(0)
        cap = cv2.VideoCapture('rtmp://192.168.81.25:1935/slot58')
        cap.set(cv2.CAP_PROP_FPS, 1)
        while True:
            ret, frame = cap.read()
            if ret:
                results = net.detect(frame)
                # self.output_image(frame, results)
                print(results)
            cv2.waitKey(1)
        cap.release()


if __name__ == '__main__':
    MinTestCommander().run()
