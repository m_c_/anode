WARNING this instruction is just a description of what do you need to do, you can run into troubles on any step
Installation

* install CUDA <https://developer.nvidia.com/cuda-downloads>
and drivers <https://www.nvidia.com/Download/index.aspx?lang=en-us>.
Paths to cuda:

 ```
 export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
 export LD_LIBRARY_PATH=/usr/local/cuda/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
 ```

* install Intel Python
<https://software.intel.com/en-us/articles/installing-intel-free-libs-and-python-apt-repo>,
then use it for creating virtual environment or all later work

 ```
 virtualenv --system-site-packages -p /opt/intel/intelpython3/bin/python venv
 ```

* `sudo apt-get install nvidia-cuda-toolkit checkinstall git supervisor default-libmysqlclient-dev`

* install opencv (4.1.0 tested)
<https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html>

 ```
 cmake -DCMAKE_BUILD_TYPE=Release \
       -DCMAKE_INSTALL_PREFIX=/usr/local \
       -DPYTHON3_EXECUTABLE=/opt/intel/intelpython3/bin/python \
       -DPYTHON_INCLUDE_DIR=/opt/intel/intelpython3/include/python3.6m \
       -DPYTHON_LIBRARY=/opt/intel/intelpython3/lib/libpython3.6m.so \
       -DPYTHON3_NUMPY_INCLUDE_DIRS=/opt/intel/intelpython3/pkgs/numpy-base-1.16.1-py36_3/lib/python3.6/site-packages/numpy/core/include \
       -DBUILD_opencv_legacy=OFF \
       -DCUDA_FAST_MATH=ON \
       -DWITH_NVCUVID=OFF \
       -DWITH_CUDA=ON \
       -DENABLE_FAST_MATH=ON \
       -DOPENCV_EXTRA_MODULES_PATH=/path/to/opencv_contrib/modules \
       ..
 make -j7
 sudo checkinstall --pkgname opencv --pkgversion 4.1.0-pre
 ```
 if you get errors about gcc version too big you can find `/usr/include/crt/host_congif.h` and comment this check 

* `pip install aiohttp psutil imutil setproctitle pika image_match requests`

* install yolo: `git clone https://github.com/pjreddie/darknet`

 set `GPU=1` and `OPENCV=1` in Makefile, if there is no file opencv.pc then create one `/usr/share/pkg-config/opencv.pc`

 ```
 prefix=/usr/local
 exec_prefix=${prefix}
 includedir=${prefix}/include
 libdir=${exec_prefix}/lib
 Name: opencv
 Description: The opencv library
 Version: 4.x.x
 Cflags: -I${includedir}/opencv4 -I${includedir}/opencv2 -I${includedir}/opencv
 Libs: -L${libdir} -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_ccalib -lopencv_cudaarithm -lopencv_cudabgsegm -lopencv_cudacodec -lopencv_cudafeatures2d -lopencv_cudafilters -lopencv_cudaimgproc -lopencv_cudalegacy -lopencv_cudaobjdetect -lopencv_cudaoptflow -lopencv_cudastereo -lopencv_cudawarping -lopencv_cudev -lopencv_datasets -lopencv_dnn -lopencv_dnn_objdetect -lopencv_dpm -lopencv_face -lopencv_freetype -lopencv_fuzzy -lopencv_gapi -lopencv_hfs -lopencv_img_hash -lopencv_imgcodecs -lopencv_imgproc -lopencv_line_descriptor -lopencv_calib3d -lopencv_imgproc -lopencv_core -lopencv_ml -lopencv_features2d -lopencv_objdetect -lopencv_flann -lopencv_video -lopencv_highgui -lopencv_optflow -lopencv_phase_unwrapping -lopencv_photo -lopencv_plot -lopencv_quality -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_shape -lopencv_stereo -lopencv_stitching -lopencv_superres -lopencv_surface_matching -lopencv_text -lopencv_tracking -lopencv_video -lopencv_videoio -lopencv_videostab -lopencv_xfeatures2d -lopencv_ximgproc -lopencv_xobjdetect -lopencv_xphoto -lopencv_structured_light
 ```

 for opencv >= 4 add this to `src/image_opencv.hpp`
 
 ```
 #include "opencv2/core/types_c.h"
 #include "opencv2/core/core_c.h"
 #include "opencv2/videoio/legacy/constants_c.h"
 #include "opencv2/highgui/highgui_c.h"
 ```

 make file:

 ```
 #!/bin/bash

 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib
 export PATH=$PATH:/usr/local/cuda/bin
 make
 ```

 this error: 
 
 ```
 /tmp/tmpxft_00003688_00000000-5_convolutional_kernels.compute_52.cudafe1.stub.c: In function ‘void __device_stub__Z15binarize_kernelPfiS_(float*, int, float*)’:
 /tmp/tmpxft_00003688_00000000-5_convolutional_kernels.compute_52.cudafe1.stub.c:13:87: error: ‘__args_arr’ was not declared in this scope
 ```
 
 can be fixed with changing `NVCC = /usr/local/cuda/bin/nvcc` in makefile

* install yolo34py-gpu from source enabling GPU and OPENCV <https://github.com/madhawav/YOLO3-4-Py>

for opencv >= 4 add this to bridge.h

 ```
 #include "opencv2/core/types_c.h"
 #include "opencv2/core/core_c.h"
 #include "opencv2/videoio/legacy/constants_c.h"
 #include "opencv2/highgui/highgui_c.h"
 ```

 and create symlink `sudo ln -s /usr/local/include/opencv4/opencv2 /usr/local/include/opencv2`
 
 change in `setup.py`: `USE_GPU = True`
 
 install script:

 ```
 export CUDA_HOME=/usr/local/cuda
 export GPU=1
 export OPENCV=1
 export DARKNET_HOME=/path/to/darknet
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DARKNET_HOME
 export CUDA_HOME=/usr/local/cuda
 pip3 install .
 ```

* install rabbitmq <https://www.rabbitmq.com/download.html>

* create some config files 
 
 config.py with minimal configuration

 ```
 from base_config import CONFIG
 CONFIG['detection']['yolo3']['cfg'] = ''
 CONFIG['detection']['yolo3']['weights'] = ''
 CONFIG['detection']['yolo3']['metadata'] = ''
 CONFIG['queue']['user'] = ''
 CONFIG['queue']['pass'] = ''
 ```

 conf file for running in supervisor

 ```
 [program:anode]
 command = python3 /work/anode/main.py
 autostart = true
 autorestart = true
 user = vadmin
 environment=LD_LIBRARY_PATH="/usr/local/cuda/lib64:/usr/lib"
 redirect_stderr = true
 stdout_logfile = /var/log/anode_std.log
 stderr_logfile= /var/log/anode_std.log
 stopsignal = QUIT
 stopwaitsecs = 540
 ```

 conf file for logrotate

 ```
 /var/log/anode.log {
     size 1M
     copytruncate
     rotate 1
     notifempty
     missingok
 }
 /var/log/anode_std.log {
     size 1M
     copytruncate
     rotate 1
     notifempty
     missingok
 }
 ```

* useful tips:

    * when changing detector don't forget to change `CONFIG['detection']['current']`
    * if manually editing videos.txt leave last line blank
    * to change message queue broker see `queues`
    * web-server commands: `/add-video, /delete-video, /get-stat, /get-screenshot`, see web.py
    * it takes many minutes for system to stop, more cameras - more time. teh fast way to do it is reboot server, but video drivers could die in the process
    * motion detector can use optional settings, like excluded areas, see motion_config

* the system works like this:

    * main process starts child processes and they do all the work
    * reader reads video frame and puts it to internal queue, shared with motion detection process 
    * motion detector gets frame from internal queue and detects motion, then if any result puts it to rabbitmq and original frame to rabbitmq for object detection
    * object detector gets frame from rabbitmq and detects objects, then cuts images with detected objects and puts them to queue for comparing
    * images comparer gets images from rabbitmq and compares to stored ones, if images are the same then they are dropped, if not - to rabbitmq again
    * somewhere must be processes that get results of all this work from rabbitmq and save it to db