from abc import ABC, abstractmethod
from typing import List, Optional, NamedTuple, Union, Dict


class AbstractQueueClient(ABC):
    ''' Abstract parent for queue clients '''

    @abstractmethod
    def init_queues(self, queues: List[str]):
        ''' Creates queues '''

    @abstractmethod
    def put(self, queue: str, item: Union[Dict, NamedTuple], do_pickle: bool = True) -> bool:
        ''' Puts to queue '''

    @abstractmethod
    def get(self, queue: str, do_pickle: bool = True) -> Union[NamedTuple, Dict, None]:
        ''' Gets from queue '''

    @abstractmethod
    def close(self):
        ''' Closes queue '''
