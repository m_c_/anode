import logging, json, pickle
from typing import List, Optional, NamedTuple, Union, Dict
import pika
from config import CONFIG
from queues.abstract import AbstractQueueClient


logger = logging.getLogger(CONFIG['logger'])


class QueueClient(AbstractQueueClient):
    ''' RabbitMQ client '''

    _connection = None # connection to queue ? channel has it
    _channel = None # channel to queue

    def __init__(self):
        ''' '''
        self._connect()

    def init_queues(self, queues: List[str]):
        ''' Creates queues '''

        for queue in queues:
            self._channel.queue_declare(queue, durable=True)

    def put(self, queue: str, item: Union[Dict, NamedTuple], do_pickle: bool = True) -> bool:
        ''' Puts to queue '''

        if do_pickle:
            body = pickle.dumps(item, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            body = json.dumps(item)

        if not self._channel.is_open or not self._connection.is_open:
            self._connect()

        try:
            self._channel.basic_publish(
                exchange='',
                routing_key=queue,
                body=body,
                mandatory=True
            )
            return True
        except pika.exceptions.ConnectionBlockedTimeout as e:
            # when queue is full
            logger.exception(e)
            return True
        except pika.exceptions.AMQPError as e:
            logger.exception(e)
            self._connect()
            return False
        except pika.exceptions.ChannelError as e:
            logger.exception(e)
            self._connect()
            return False

    def get(self, queue: str, do_pickle: bool = True) -> Union[NamedTuple, Dict, None]:
        ''' Gets from queue '''

        if not self._channel.is_open or not self._connection.is_open:
            self._connect()

        try:
            method_frame, header_frame, body = self._channel.basic_get(queue, True)
            if method_frame:
                if do_pickle:
                    return pickle.loads(body)
                else:
                    return json.loads(body)
        except pika.exceptions.AMQPError as e:
            logger.exception(e)
            self._connect()

        return None

    def close(self):
        ''' Closes queue '''
        try:
            self._channel.close()
            self._connection.close()
        except Exception as e:
            logger.exception(e)

    def process_events(self):
        ''' '''
        self._connection.process_data_events(1)

    def sleep(self, period: int):
        ''' '''
        self._connection.sleep(period)

    def _connect(self):
        ''' Connects to queue '''

        self._connection = pika.BlockingConnection(
            pika.URLParameters(
                'amqp://{0}:{1}@{2}:{3}?blocked_connection_timeout={4}&heartbeat={5}'.format(
                    CONFIG['queue']['user'],
                    CONFIG['queue']['pass'],
                    CONFIG['queue']['host'],
                    CONFIG['queue']['port'],
                    CONFIG['queue']['blocked_timeout'],
                    180 # @TODO
                )
            )
        )
        self._channel = self._connection.channel()
